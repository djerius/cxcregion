
/*                                                                
**  Copyright (C) 2007  Smithsonian Astrophysical Observatory 
*/                                                                

/*                                                                          */
/*  This program is free software; you can redistribute it and/or modify    */
/*  it under the terms of the GNU General Public License as published by    */
/*  the Free Software Foundation; either version 3 of the License, or       */
/*  (at your option) any later version.                                     */
/*                                                                          */
/*  This program is distributed in the hope that it will be useful,         */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/*  GNU General Public License for more details.                            */
/*                                                                          */
/*  You should have received a copy of the GNU General Public License along */
/*  with this program; if not, write to the Free Software Foundation, Inc., */
/*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.             */
/*                                                                          */


 /* 
  * Collection of basic utility functions shared between
  * reglib tests.
  */

void printStatus(char*, char*, short);
void printMask(regRegion*);

void printStatus( char* name, char* expected, short status ) {
    printf( "Is point inside?  %s = %s (expected=%s)\n", name, status ? "yes" : "no", expected );
}

void printMask( regRegion* myRegion ) {
    
    short *mask; long xlen; long ylen; short ii; short jj;

    regRegionToMask( myRegion, 0, 9, 0, 9, 1, &mask, &xlen, &ylen );
    for (ii=ylen-1; ii>=0; ii--) 
    {
        for (jj=0; jj<xlen; jj++)
        {
            printf( "%c ", mask[jj+ii*xlen] == 0 ? 'O' : 'X' );
        }
        printf("\n");
    }        

    if (mask) free (mask); 

}
