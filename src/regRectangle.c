#include "region_priv.h"


regShape* regCopyRectangle( regShape * );
int regIsEqualRectangle( regShape *, regShape * );
double regCalcAreaRectangle( regShape * );
int regCalcExtentRectangle( regShape *, double *, double * );
int regInsideRectangle( regShape *, double, double );
void regToStringRectangle( regShape *, char*, long );


regShape* regCreateRectangle(regFlavor include, 
			     const double *xpos, 
			     const double *ypos,
			     int wcoord,
                 int wsize) {

  if ((xpos == NULL) || (ypos == NULL)) {
    fprintf( stderr, "ERROR: regCreateRectangle() requires [xpos, ypos] coordinate pair.");
    return NULL;
  }

  regShape* newShape;
  long npoints = 2;

  newShape = ( regShape *) calloc ( 1, sizeof( regShape ) );
  newShape->name = "Rectangle";
  newShape->flag_coord = wcoord;
  newShape->flag_radius = wsize;

  newShape->xpos = ( double *)calloc( npoints, sizeof(double ));
  newShape->ypos = ( double *)calloc( npoints, sizeof(double) );

  long ii;
  for ( ii=0; ii<npoints;ii++)
    {
      newShape->xpos[ii] = xpos[ii]; 
      newShape->ypos[ii] = ypos[ii];
    }

  newShape->include = include;
  newShape->nPoints = npoints;
 
  newShape->type = regRECTANGLE;

  newShape->radius = NULL;

  newShape->calcArea   = regCalcAreaRectangle;
  newShape->calcExtent = regCalcExtentRectangle;
  newShape->copy       = regCopyRectangle;
  newShape->isEqual    = regIsEqualRectangle;
  newShape->isInside   = regInsideRectangle;
  newShape->toString   = regToStringRectangle;
  newShape->free       = NULL;

  newShape->region = NULL;
  newShape->next   = NULL;

  return newShape;

}

// copy
regShape* regCopyRectangle( regShape* inShape ) {

  if (inShape == NULL) {
    fprintf( stderr, "ERROR: regCopyRectangle() requires a regShape as input");
    return NULL;
  }
  
  if (inShape->type != regRECTANGLE) {
    fprintf( stderr, "ERROR: regCopyRectangle() incorrect regShape type");
    return NULL;
  }

  return regCreateRectangle( inShape->include, inShape->xpos, 
			     inShape->ypos,
                             inShape->flag_coord, inShape->flag_radius);
}


// equals
int regIsEqualRectangle( regShape* thisShape, regShape* otherShape ) {
    
  if (!thisShape && !otherShape) {
      return 1;
  }

  if (!thisShape || !otherShape) {
      return 0;
  }

  if ( (thisShape->type != regRECTANGLE) ) { 
    fprintf( stderr, "ERROR: regIsEqualRectangle() unable to compare shapes of different types");
    return 0;
  }

  if ( otherShape->type != regRECTANGLE ) {
    return 0;
  }

  if ( thisShape->xpos == NULL ||  otherShape->xpos == NULL ||
       thisShape->ypos == NULL ||  otherShape->ypos == NULL  ) {
    fprintf( stderr, "ERROR: regIsEqualRectangle() unable to compare incomplete shapes");
    return 0;
  }

  if ( (thisShape->xpos[0] != otherShape->xpos[0]) ||
       (thisShape->xpos[1] != otherShape->xpos[1]) ||
       (thisShape->ypos[0] != otherShape->ypos[0]) ||
       (thisShape->ypos[1] != otherShape->ypos[1]) ||
       (thisShape->include != otherShape->include) ||
       (thisShape->flag_coord != otherShape->flag_coord) ||
       (thisShape->flag_radius != otherShape->flag_radius) )
    return 0;

  return 1;
}


// calcArea
double regCalcAreaRectangle( regShape* shape ) {

    if (shape == NULL) {
      fprintf( stderr, "ERROR: regCalcAreaRectangle() requires a regShape as input");
      return 0;
    }

    if (shape->type != regRECTANGLE) {
      fprintf( stderr, "ERROR: regCalcAreaRectangle() incorrect regShape type");
      return 0;
    }

    double area;
    double xr = shape->xpos[1] - shape->xpos[0];
    double yr = shape->ypos[1] - shape->ypos[0];

    area = fabs( xr * yr );
    return area;

}


// calcExtent
int regCalcExtentRectangle( regShape* shape, double* xpos, double* ypos ) {
    if (shape == NULL) {
      fprintf( stderr, "ERROR: regCalcExtentRectangle() requires a regShape as input");
      return 0;
    }
    
    if (shape->type != regRECTANGLE) {
      fprintf( stderr, "ERROR: regCalcExtentRectangle() incorrect regShape type");
      return 0;
    }
    
    if ((xpos == NULL) || (ypos == NULL)) {
      fprintf( stderr, "ERROR: regCalcExtentRectangle() requires pre-allocated memory for xpos and ypos");
      return 0;
    }
    
    xpos[0] = shape->xpos[0];
    ypos[0] = shape->ypos[0];

    xpos[1] = shape->xpos[1];
    ypos[1] = shape->ypos[1];

    return 0;
}


// inside
int regInsideRectangle( regShape* shape, double xpos, double ypos ) {
  
  if (shape == NULL) {
    fprintf( stderr, "ERROR: regInsideRectangle() requires a regShape as input");
    return 0;
  }

  if (shape->type != regRECTANGLE) {
    fprintf( stderr, "ERROR: regInsideRectangle() incorrect regShape type");
    return 0;
  }

  int retval;

  if ( ( xpos >= shape->xpos[0] ) &&
       ( xpos <= shape->xpos[1] ) &&
       ( ypos >= shape->ypos[0] ) &&
       ( ypos <= shape->ypos[1] ) )
    retval = 1;
  else
    retval = 0;

  if ( shape->include == regInclude )
    return(retval );
  else
    return( 1 - retval );

}


// toString
void regToStringRectangle( regShape* shape, char* buf, long maxlen ) {

  if (shape == NULL) {
    fprintf( stderr, "ERROR: regToStringRectangle() requires a regShape as input");
    return;
  }

  if (shape->type != regRECTANGLE) {
    fprintf( stderr, "ERROR: regToStringRectangle() incorrect regShape type");
    return;
  }

  if (shape->include == regExclude) {
    *buf++ = '!';
  }
  
  long bsize = 80;

  char *x1 = calloc(bsize, sizeof(char));
  char *y1 = calloc(bsize, sizeof(char));
  reg_print_pos_pair(shape->xpos[0], shape->ypos[0], 
            shape->flag_coord, x1, y1);

  char *x2 = calloc(bsize, sizeof(char));
  char *y2 = calloc(bsize, sizeof(char));
  reg_print_pos_pair(shape->xpos[1], shape->ypos[1], 
            shape->flag_coord, x2, y2);
  
  snprintf(buf, maxlen, "Rectangle(%s,%s,%s,%s)", x1, y1, x2, y2);
  
  free(x1);
  free(y1);
  free(x2);
  free(y2);
}
