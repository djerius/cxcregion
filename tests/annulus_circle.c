#include <float.h>
#include <math.h>

#include "acutest.h"

#include "cxcregion.h"
#include "region_priv.h"

#include "common.h"

Setup
setup () {

    INIT_SETUP( "Annulus" );

    XCEN_(8);
    YCEN_(10);

    XPOS_( XCEN );
    YPOS_( YCEN );
    RADIUS_( 0, 33 );

    AREA_( M_PI * (RADIUS[1]*RADIUS[1] - RADIUS[0]*RADIUS[0]) );
    XBND_( XCEN - RADIUS[1], XCEN + RADIUS[1] );
    YBND_( YCEN - RADIUS[1], YCEN + RADIUS[1] );
    PTS_( { XCEN, YCEN, INSIDE },
          { XCEN + (RADIUS[1] + RADIUS[0] ) / 2, YCEN + (RADIUS[1] + RADIUS[0] ) / 2, INSIDE },
          { XCEN + RADIUS[0], YCEN + RADIUS[0], INSIDE },
          { XCEN + RADIUS[1], YCEN + RADIUS[1], OUTSIDE },
        );

#include "annulus_circle.h"
 
    DSL_( "Annulus(%.0f,%.0f,%.0f,%.0f)", XCEN,YCEN,RADIUS[0],RADIUS[1] );
 

    FINALIZE_SETUP;
}

void test_factory( ) {
    common_test_factory( setup() );
}

void test_dsl() {
    common_test_dsl( setup() );
}

TEST_LIST = {

    { "factory", test_factory },
    { "dsl", test_dsl },
    { NULL, NULL }
};
