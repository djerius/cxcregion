#include <float.h>
#include <math.h>

#include "acutest.h"
#include "cxcregion.h"
#include "region_priv.h"

#include "common.h"


Setup
setup () {

    INIT_SETUP( "RotBox" );

    XCEN_(8);
    YCEN_(10);
    XLEN_(20);
    YLEN_(10);

    XPOS_(XCEN);
    YPOS_(YCEN);
    RADIUS_( XLEN, YLEN );
    ANGLE_(45);

    AREA_( XLEN * YLEN );

#include "box.h"


    BBox bbox = bound_rotated_box( XLEN, YLEN, ANGLE[0] );
    XBND_( XCEN + bbox.ll.x, XCEN + bbox.ur.x );
    YBND_( YCEN + bbox.ll.y, YCEN + bbox.ur.y );
    
    /* box corners should be inside the box, but because of round-off
     * the tests sometimes fail.  choose points just inside */
    Box in = rotated_box( XLEN * ONE_MINUS, YLEN * ONE_MINUS, ANGLE[0] );

    /* Now, choose points just outside.  */
    Box out = rotated_box( XLEN * ONE_PLUS, YLEN * ONE_PLUS , ANGLE[0] );

    PTS_(
        { XCEN + in.ll.x, YCEN + in.ll.y, INSIDE },
        { XCEN + in.ul.x, YCEN + in.ul.y, INSIDE },
        { XCEN + in.ur.x, YCEN + in.ur.y, INSIDE },
        { XCEN + in.lr.x, YCEN + in.lr.y, INSIDE },
        { XCEN + out.ll.x, YCEN + out.ll.y, OUTSIDE },
        { XCEN + out.ul.x, YCEN + out.ul.y, OUTSIDE },
        { XCEN + out.ur.x, YCEN + out.ur.y, OUTSIDE },
        { XCEN + out.lr.x, YCEN + out.lr.y, OUTSIDE },
        );

    if ( ANGLE[0] == 0.0 ) 
        DSL_( "Box(%g,%g,%g,%g)", XCEN,YCEN,XLEN,YLEN );
    else
        DSL_( "RotBox(%g,%g,%g,%g,%g)", XCEN,YCEN,XLEN,YLEN,ANGLE[0] );

    FINALIZE_SETUP;
}

void test_factory( ) {
    common_test_factory( setup() );
}

void test_dsl() {
    common_test_dsl( setup() );
}

TEST_LIST = {

    { "factory", test_factory },
    { "dsl", test_dsl },
    { NULL, NULL }
};
