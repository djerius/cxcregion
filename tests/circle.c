#include <float.h>
#include <math.h>

#include "acutest.h"

#include "cxcregion.h"
#include "region_priv.h"

#include "common.h"

Setup
setup () {

    INIT_SETUP( "Circle" );


    XCEN_(8);
    YCEN_(10);
    RADIUS_( 44 );
    ANGLE_();

    XPOS_(XCEN);
    YPOS_(YCEN);


    AREA_( M_PI * RADIUS[0] * RADIUS[0] );
    XBND_(XCEN - RADIUS[0], XCEN + RADIUS[0]);
    YBND_(YCEN - RADIUS[0], YCEN + RADIUS[0]);
    PTS_(
        /* Circle Center */
        { XCEN, YCEN, INSIDE },

        /* on the circle along the axes */
        { XCEN + RADIUS[0], YCEN, INSIDE },
        { XCEN - RADIUS[0], YCEN, INSIDE },
        { XCEN, YCEN + RADIUS[0], INSIDE },
        { XCEN, YCEN - RADIUS[0], INSIDE },

        /* on the circle at 45 degrees */
        { XCEN - RADIUS[0] * M_SQRT1_2, YCEN - RADIUS[0] * M_SQRT1_2, INSIDE },
        { XCEN - RADIUS[0] * M_SQRT1_2, YCEN + RADIUS[0] * M_SQRT1_2, INSIDE },
        { XCEN + RADIUS[0] * M_SQRT1_2, YCEN + RADIUS[0] * M_SQRT1_2, INSIDE },
        { XCEN + RADIUS[0] * M_SQRT1_2, YCEN - RADIUS[0] * M_SQRT1_2, INSIDE },

        { XCEN + RADIUS[0] / 2, YCEN + RADIUS[0] / 2, INSIDE },

        );
#include "circle.h"

    DSL_( "Circle(%g,%g,%g)", XCEN,YCEN,RADIUS[0] );


    FINALIZE_SETUP;
}

void test_factory( ) {
    common_test_factory( setup() );
}

void test_dsl() {
    common_test_dsl( setup() );
}


TEST_LIST = {

    { "factory", test_factory },
    { "dsl", test_dsl },
    { NULL, NULL }
};
