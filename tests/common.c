#include <math.h>
#include <float.h>
#include <stddef.h>
#include <string.h>
#include <ctype.h>
#include "cxcregion.h"
#include "region_priv.h"


#define TEST_NO_MAIN 1
#include "acutest.h"

#include "tests/common.h"

static const double FIELDX[] = { -DBL_MAX, DBL_MAX };
static const double FIELDY[] = { -DBL_MAX, DBL_MAX };

double deg2rad( double theta ) {
    return theta * M_PI / 180;
}

static Point
rotate ( double x, double y, double theta ) {
    Point rotated;
    double dtheta = deg2rad( theta );

    rotated.x = x * cos( dtheta ) - y * sin( dtheta );
    rotated.y = x * sin( dtheta ) + y * cos( dtheta );
    return rotated;
}

#define MIN(x,y) ( x < y ? x : y )
#define MAX(x,y) ( x > y ? x : y )

#define MINBND( c, box )  MIN( MIN( box[0].c, box[1].c ), MIN( box[2].c, box[3].c ) )
#define MAXBND( c, box )  MAX( MAX( box[0].c, box[1].c ), MAX( box[2].c, box[3].c ) )

static void
fprintf_point( FILE* stream,  Point pt ) {
    fprintf( stream, "x = %g\ty = %g\n", pt.x, pt.y );
}

static void
fprintf_box( FILE* stream, Box box ) {

    fprintf( stream, "ll:" );
    fprintf_point( stream, box.ll );
    fprintf( stream, "ul:" );
    fprintf_point( stream,  box.ul );
    fprintf( stream, "ur:" );
    fprintf_point( stream, box.ur );
    fprintf( stream, "lr:" );
    fprintf_point( stream, box.lr );
}

static void
fprintf_mask( FILE* stream, Mask mask ) {

    const short* ptr = mask.val;
    long ix, iy;

    for ( iy = 0 ; iy < mask.ylen ; ++iy ) {
        for ( ix = 0 ; ix < mask.xlen-1 ; ++ix, ++ptr ) {
            fprintf( stream, "%d, ", *ptr);
        }
        fprintf( stream, "%d,\n", *ptr++ );
    }
}

static void
fprintf_header_name( FILE* stream, const char* filename ) {

    long len = strlen( filename );

    for( ; len ; --len, ++filename ) {
        char c = *filename == '.' ? '_' : *filename;
        fputc( toupper( c ), stream  );
    }
}

static void
generate_mask_file( const char* filename, Mask mask ) {

    FILE* stream;

    char buf[1024];

    {
        const char* start = rindex( filename, '/' );
        const char* end = rindex( filename, '.' );

        start = start == NULL ? filename : start + 1;
        size_t length = end == NULL
            ? strlen( start )
            : end - start;

        strncpy( buf, start, length );
        buf[length] = '\0';
        strcat( buf, ".h" );
    }
    fprintf( stderr, "FILENAME = %s\n", buf );

    stream = fopen( buf, "w" );


    TEST_ASSERT( stream != NULL );

    fputs( "#ifndef ", stream);
    fprintf_header_name( stream, buf );
    fputc( '\n', stream );
    fputs( "#define ", stream);
    fprintf_header_name( stream, buf );
    fputc( '\n', stream );

    fputs( "#include \"common.h\"", stream );
    fputc( '\n', stream );
    fputc( '\n', stream );

    fputs( "static const short mask_val[] = {\n", stream );
    fprintf_mask( stream, mask );
    fputs( "};\n", stream );
    fprintf( stream, "static const Mask MASK = { mask_val, %ld, %ld};\n", mask.xlen, mask.ylen );

    fputc( '\n', stream );
    fputs( "#endif /* ! ", stream);
    fprintf_header_name( stream, buf );
    fputs( " */\n", stream );

    fclose( stream );
}

Box
rotated_box ( double xlen, double ylen, double theta ) {
    Box box;

    double xmin = -xlen / 2;
    double ymin = -ylen / 2;
    double xmax =  xlen / 2;
    double ymax =  ylen / 2;

    box.ll = rotate( xmin, ymin, theta );
    box.ul = rotate( xmin, ymax, theta );
    box.ur = rotate( xmax, ymax, theta );
    box.lr = rotate( xmax, ymin, theta );

    return box;
}

BBox
bound_rotated_box ( double xlen, double ylen, double theta ) {

    Point cnr[4];

    Box box = rotated_box( xlen, ylen, theta );
    cnr[0] = box.ll;
    cnr[1] = box.ul;
    cnr[2] = box.ur;
    cnr[3] = box.lr;

    BBox bbox;
    bbox.ll.x = MINBND(x, cnr);
    bbox.ll.y = MINBND(y, cnr);
    bbox.ur.x = MAXBND(x, cnr);
    bbox.ur.y = MAXBND(y, cnr);
    return bbox;

}


#define TEST_CHECK_MSG( got, expected, test  )          \
    do {                                                \
        TEST_CHECK_( (got) == (expected), test );       \
        TEST_MSG( "Expected: %g", expected );           \
        TEST_MSG( "Got     : %g", got );                \
    } while(0)

#define TEST_EXTENT( gotX, gotY, expX, expY, test  )    \
    do {                                                \
        TEST_CHECK_MSG( (gotX[0]), (expX[0]), "extent: xmin" ); \
        TEST_CHECK_MSG( (gotX[0]), (expX[0]), "extent: xmax" ); \
        TEST_CHECK_MSG( (gotY[0]), (expY[0]), "extent: ymin" ); \
        TEST_CHECK_MSG( (gotY[0]), (expY[0]), "extent: ymax" ); \
    } while(0)


/* test whether extent contains the entire shape by
   1. generate an oversize mask, checking that the outer couple of rows and columns are all zero.
      That indicates its big enough to contain the entire shape.
   2. generate a mask based on the extents, and compare the number of set elements in it to that
      in the oversized mask.

   This should work, modulo the bin size
*/
static void
test_extents_with_mask( regRegion* region, BBox extent ) {

    long big_sum, extent_sum;

    {
        BBox bbig = extent;
        short *mask;
        long xlen, ylen;
        int retval;
        bbig.ll.x -= 2;
        bbig.ll.y -= 2;
        bbig.ur.x += 2;
        bbig.ur.y += 2;

        retval = regRegionToMask( region, bbig.ll.x, bbig.ur.x, bbig.ll.y, bbig.ur.y, 0.1, &mask, &xlen, &ylen );
        TEST_ASSERT( retval == 0 );

        /* test "frame" */
        {
            short *ptr;
            short *end;
            long sum;
            long iy;

            ptr = mask;
            end = ptr + 2 * xlen ;
            for( sum = 0 ; ptr < end ; ++ptr )
                sum += *ptr;

            TEST_CHECK_( sum == 0, "top two rows of big mask are empty" );

            ptr = mask + ( ylen - 2 ) * xlen;
            end = ptr + 2 * xlen ;
            for( sum = 0 ; ptr < end ; ++ptr )
                sum += *ptr;

            TEST_CHECK_( sum == 0, "bottom two rows of big mask are empty" );

            ptr = mask;
            for( iy = 0; iy < ylen ; ++iy, ptr += xlen )
                sum += ptr[0] + ptr[1];

            TEST_CHECK_( sum == 0, "left two columns of big mask are empty" );

            ptr = mask + xlen - 2;
            for( iy = 0; iy < ylen ; ++iy, ptr += xlen )
                sum += ptr[0] + ptr[1];

            TEST_CHECK_( sum == 0, "right two columns of big mask are empty" );

            ptr = mask;
            end = ptr + xlen * ylen;
            for( big_sum = 0 ; ptr < end ; ++ptr )
                big_sum += *ptr;

            TEST_CHECK_( big_sum != 0, "big mask contains lit pixels" );
        }

        free( mask );

    }

    {
        short *mask;
        long xlen, ylen;
        int retval;

        retval = regRegionToMask( region, extent.ll.x, extent.ur.x, extent.ll.y, extent.ur.y, 0.1, &mask, &xlen, &ylen );
        TEST_ASSERT( retval == 0 );

        {
            short *ptr = mask;
            short *end = ptr + xlen * ylen;
            for( extent_sum = 0 ; ptr < end ; ++ptr )
                extent_sum += *ptr;

            TEST_CHECK_( big_sum != 0, "extent mask contains lit pixels" );

        }

        free( mask );
    }


    TEST_CHECK_( big_sum == extent_sum, "extent seems to contain all of shape" );
}


static void
test_mask( Mask got, Mask expected ) {

    TEST_ASSERT_( got.xlen == expected.xlen, "Mask xlen" );
    TEST_ASSERT_( got.ylen == expected.ylen, "Mask ylen" );

    {
        const short* gm = got.val;
        const short* em = expected.val;

        size_t nelem = got.xlen * got.ylen;
        size_t cnt;

        for( cnt = nelem ; cnt ; --cnt, ++gm, ++em  ) {
            if ( *gm != *em ) {
                size_t n = nelem - cnt;
                size_t iy = n / got.xlen;
                size_t ix = n - iy * got.xlen;
                TEST_CHECK_( 0, "Mask[%ld,%ld]: ", ix, iy );
                TEST_MSG( "Expected: %d", *em );
                TEST_MSG( "Got     : %d", *gm );
            }
        }
    }
}

static regRegion* new_from_factory(  FactoryArgs args ) {

    regRegion* region = regCreateEmptyRegion();
    TEST_ASSERT_( region != NULL, "empty region was created" );

    regAppendShape( region, args.shape, regInclude, regAND,
                    args.xpos, args.ypos, args.npoints, args.radius, args.angle, args.world_coord, args.world_size );

    return region;
}

static void test_inside_shape( const char* label, regShape* shape, const TestPoint tp[], int npts ) {
    int idx;
    for ( idx = 0 ; idx < npts ; ++idx ) {
        double x = tp[idx].x;
        double y = tp[idx].y;
        int inside = shape->isInside( shape, x, y );

        char *expected = tp[idx].inside.flag ? "inside" : "outside";

        TEST_CHECK_( inside == tp[idx].inside.flag, "%s: pt #%d: (%g, %g) = %s", label, idx, x, y, expected );
    }
}

static void test_inside_region( const char* label, regRegion* region, const TestPoint tp[], int npts ) {
    int idx;
    for ( idx = 0 ; idx < npts ; ++idx ) {
        double x = tp[idx].x;
        double y = tp[idx].y;
        int inside = regInsideRegion( region, x, y );

        char *expected = tp[idx].inside.flag ? "inside" : "outside";

        TEST_CHECK_( inside == tp[idx].inside.flag, "%s: pt #%d: (%g, %g) = %s", label, idx, x, y, expected );
    }
}

static BBox new_bbox_from_bounds( double xbnd[], double ybnd[] ) {
    BBox bbox;
    bbox.ll.x = xbnd[0];
    bbox.ll.y = ybnd[0];
    bbox.ur.x = xbnd[1];
    bbox.ur.y = ybnd[1];
    return bbox;
}

static void bbox_to_points( BBox bbox, TestPoint pts[], MyBool inside, double factor ) {

        pts[0].x      = bbox.ll.x * factor;
        pts[0].y      = bbox.ll.y * factor;
        pts[0].inside = inside;

        pts[1].x      = bbox.ll.x * factor;
        pts[1].y      = bbox.ur.y * factor;
        pts[1].inside = inside;

        pts[2].x      = bbox.ur.x * factor;
        pts[2].y      = bbox.ur.y * factor;
        pts[2].inside = inside;

        pts[3].x      = bbox.ur.x * factor;
        pts[3].y      = bbox.ll.y * factor;
        pts[3].inside = inside;
}

static void test_shape ( regShape* shape, const ExpectedValues expected ) {
    TEST_CASE( "shape" );
    TEST_ASSERT_( shape != NULL, "retrieved shape" );
    TEST_CHECK_MSG( shape->calcArea( shape ), expected.shape_area, "area" );

    /* infinite or semi-infinite regions don't have extents, which is
     * indicated by all zeroes in the expected bounding box */
    if ( ! ( 
                expected.xbnd[0] == 0
             && expected.xbnd[1] == 0
             && expected.ybnd[0] == 0
             && expected.ybnd[1] == 0
             )
        ) {
        double xbnd[2];
        double ybnd[2];
        shape->calcExtent(shape, xbnd, ybnd );
        TEST_EXTENT( xbnd, ybnd, expected.xbnd, expected.ybnd, "extent" );
        BBox bbox = new_bbox_from_bounds( xbnd, ybnd );

        TestPoint pts[4];
        bbox_to_points( bbox, pts, OUTSIDE, ONE_PLUS );
        test_inside_shape( "extent", shape, pts, 4 );
    }

    if ( expected.dsl ) {
        char got[1024];
        shape->toString(shape, got, sizeof(got) -1 );
        TEST_CHECK_( strcmp( got, expected.dsl ) == 0, "string representaton" );
        TEST_MSG( "Expected: %s", expected.dsl );
        TEST_MSG( "Got     : %s", got );
    }

    test_inside_shape( "fiducial points", shape, expected.tp, expected.npts );

}

static void test_region( regRegion* region, const ExpectedValues expected ) {
    TEST_ASSERT_( regGetNoShapes( region ) == 1, "one shape in region" );

    TEST_CHECK_MSG( regArea( region, FIELDX, FIELDY, 1 ), expected.region_area, "region area" );

    /* infinite or semi-infinite regions don't have extents, which is
     * indicated by all zeroes in the expected bounding box */
    if ( ! ( 
                expected.xbnd[0] == 0
             && expected.xbnd[1] == 0
             && expected.ybnd[0] == 0
             && expected.ybnd[1] == 0
             )
        ) {
        double xbnd[2];
        double ybnd[2];
        regExtent(region, FIELDX, FIELDY, xbnd, ybnd );
        TEST_EXTENT( xbnd, ybnd, expected.xbnd, expected.ybnd, "region extent" );

        {
            BBox bbox = new_bbox_from_bounds( xbnd, ybnd );
            TestPoint pts[4];
            bbox_to_points( bbox, pts, OUTSIDE, ONE_PLUS );
            test_inside_region( "extent", region, pts, 4 );

            test_extents_with_mask( region, bbox );
        }

        {
            Mask mask;
            short *val;
            int retval = regRegionToMask( region, xbnd[0]-1, xbnd[1]+1, ybnd[0]-1, ybnd[1]+1, 1, &val, &mask.xlen, &mask.ylen );
            mask.val = val;
            TEST_ASSERT( retval == 0 );

#ifdef GENERATE_MASK_FILE
            generate_mask_file( expected.filename, mask );
#endif
            test_mask( mask, expected.mask );
            free( val );
        }
    }

    if ( expected.dsl ) {
        char *got = regToStringRegion(region);
        TEST_CHECK_( strcmp( got, expected.dsl ) == 0, "string representaton" );
        TEST_MSG( "Expected: %s", expected.dsl );
        TEST_MSG( "Got     : %s", got );
        free( got );
    }

    test_inside_region( "fiducial points", region, expected.tp, expected.npts );

    test_shape( regGetShapeNo( region, 1 ), expected );
}

static void check_setup( const ExpectedValues expected) {
    TEST_CHECK( expected.setup.flag == TRUE.flag );
}

void common_test_factory( const Setup setup ) {

    check_setup(setup.expected);

    {
        regRegion* region = new_from_factory( setup.args );
        test_region( region, setup.expected );
        regFree( region );
    }
}

void common_test_dsl( const Setup setup ) {

    check_setup(setup.expected);

    {
        char buffer[1024];
        strcpy( buffer, setup.expected.dsl );

        regRegion* region = regParse( buffer );
        TEST_ASSERT_( region != NULL, "region was created from DSL" );

        test_region( region, setup.expected );
        regFree( region );
    }

}


