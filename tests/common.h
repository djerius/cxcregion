#ifndef TESTS_COMMON_H
#define TESTS_COMMON_H

#include <stddef.h>
#include <cxcregion.h>

struct Point { double x; double y; };
typedef struct Point Point;

struct bbox { Point ll; Point ur; };
typedef struct bbox BBox;

struct box {
    Point ll;
    Point ul;
    Point ur;
    Point lr;
};
typedef struct box Box;

/* enforce correct values via types */
struct mybool { int flag; };
typedef struct mybool MyBool;

static const MyBool INSIDE = { 1 };
static const MyBool OUTSIDE = { 0 };

static const MyBool TRUE = { 1 };
static const MyBool FALSE = { 0 };

struct test_point {
    double x;
    double y;
    MyBool inside;
};

typedef struct test_point TestPoint;

struct mask {
    const short* val;
    long xlen;
    long ylen;
};
typedef struct mask Mask;
#define EMPTY_MASK { NULL, 0, 0 }

struct FactoryArgs_ {
    char shape[100];
    double xpos[100];
    double ypos[100];
    long npoints;
    double radius[2];
    double angle[2];
    int world_coord;
    int world_size;
} ;

typedef struct FactoryArgs_ FactoryArgs;

struct ExpectedValues_ {
    MyBool setup;
    double shape_area;
    double region_area;
    double xbnd[2];
    double ybnd[2];
    char dsl[1024];
    TestPoint tp[100];
    int npts;
    Mask mask;
    char filename[1024];
};
typedef struct ExpectedValues_ ExpectedValues;

struct Setup_ {
    const FactoryArgs args;
    const ExpectedValues expected;
};
typedef struct Setup_ Setup;


#define COPY(dest,src) do { memcpy( dest, src, sizeof( src ) ); } while(0)

#define XCEN_( value ) const double XCEN = value
#define YCEN_( value ) const double YCEN = value
#define XLEN_( value ) const double XLEN = value
#define YLEN_( value ) const double YLEN = value
#define AREA_(value )  const double SHAPE_AREA = value; const double REGION_AREA = value;
#define SHAPE_AREA_(value )  const double SHAPE_AREA = value
#define REGION_AREA_(value )  const double REGION_AREA = value

#define XMIN_( value ) const double XMIN = value
#define YMIN_( value ) const double YMIN = value

#define XMAX_( value ) const double XMAX = value
#define YMAX_( value ) const double YMAX = value

#define COS( ANGLE ) cos(deg2rad(ANGLE))
#define SIN( ANGLE ) sin(deg2rad(ANGLE))

#define XBND_( ... )   do {                     \
    const double XBND[] = {__VA_ARGS__};        \
    COPY(Expected.xbnd, XBND );                 \
    } while(0)
    
#define YBND_( ... )   do {                     \
    const double YBND[] = {__VA_ARGS__};        \
    COPY(Expected.ybnd, YBND );                 \
    } while(0)
    
#define XPOS_( ... )   do {                             \
        double XPOS[] = {__VA_ARGS__};                  \
        COPY(Args.xpos, XPOS);                          \
        Args.npoints = sizeof(XPOS) / sizeof(*XPOS);    \
    } while (0)

#define YPOS_( ... )   do {                             \
        double YPOS[] = {__VA_ARGS__};                  \
        COPY(Args.ypos, YPOS);                          \
        Args.npoints = sizeof(YPOS) / sizeof(*YPOS);    \
    } while (0)

#define RADIUS_( ... )   do {                   \
        double RADIUS[] = {__VA_ARGS__};        \
        COPY(Args.radius, RADIUS);              \
    } while (0)

#define ANGLE_( ... )   do {                   \
        double ANGLE[] = {__VA_ARGS__};        \
        COPY(Args.angle, ANGLE);              \
    } while (0)

#define PTS_(...) do {                                          \
    const TestPoint PTS[] = {__VA_ARGS__};                      \
    COPY(Expected.tp, PTS );                                    \
    Expected.npts        = sizeof(PTS)/sizeof(*PTS);            \
    } while (0)

#define MASK_(...)     const Mask MASK = {__VA_ARGS__}
#define NPTS ( sizeof(PTS) / sizeof(TestPoint) )
#define DSL_(...)      do { sprintf( Expected.dsl, __VA_ARGS__ ); } while(0)

#define EMPTY_BND 0, 0


#define INIT_SETUP(SHAPE)                                               \
    FactoryArgs Args;                                                   \
    ExpectedValues Expected = { FALSE, -1, -1, { EMPTY_BND }, { EMPTY_BND }, {'\0'} , {}, -1, EMPTY_MASK, {'\0'} }; \
    const double *XPOS   = Args.xpos;                                   \
    const double *YPOS   = Args.ypos;                                   \
    const double *XBND   = Expected.xbnd;                               \
    const double *YBND   = Expected.ybnd;                               \
    const double *RADIUS = Args.radius;                                 \
    const double *ANGLE  = Args.angle;                                  \
    do {                                                                \
        strcpy(Args.shape, SHAPE );                                     \
        Args.world_coord = RC_PHYSICAL;                                 \
        Args.world_size  = RC_PHYSICAL;                                 \
        Args.npoints     = 0;                                           \
    } while(0)


#define FINALIZE_SETUP                          \
    do {                                        \
        Expected.setup       = TRUE;            \
        Expected.shape_area  = SHAPE_AREA;      \
        Expected.region_area = REGION_AREA;     \
        Expected.mask        = MASK;            \
        strcpy(Expected.filename, __FILE__);    \
        {                                       \
            Setup setup = { Args, Expected };   \
            return setup;                       \
        }                                       \
    } while(0)



static const double ONE_PLUS  = 1 + 1e-8;
static const double ONE_MINUS = 1 - 1e-8;

#define DEFAULT_SETUP static void setup() { SETUP_RAN; }


void common_test_factory( const Setup setup );
void common_test_dsl( const Setup setup );
BBox bound_rotated_box ( double xlen, double ylen, double theta );
Box rotated_box ( double xlen, double ylen, double theta );

double deg2rad( double theta );

#endif /* ! TESTS_COMMON_H */
