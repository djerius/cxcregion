#include <float.h>
#include <math.h>

#include "acutest.h"

#include "cxcregion.h"
#include "region_priv.h"

#include "common.h"

Setup
setup () {

    INIT_SETUP( "Ellipse" );


    XCEN_( 8 );
    YCEN_( 10 );
    XPOS_( XCEN );
    YPOS_( YCEN );
    RADIUS_( 11, 33 );
    ANGLE_( 44);
    const double COS_ANGLE[] = { cos( deg2rad( ANGLE[0] ) ) };
    const double SIN_ANGLE[] = { sin( deg2rad( ANGLE[0] ) ) };

    AREA_( M_PI * RADIUS[0] * RADIUS[1] );
    XBND_( EMPTY_BND );
    YBND_( EMPTY_BND );


    PTS_(
        { XCEN, YCEN, INSIDE },

        { XCEN + ONE_MINUS * RADIUS[0] * COS_ANGLE[0], ONE_MINUS * YCEN + RADIUS[0] * SIN_ANGLE[0], INSIDE },
        { XCEN - ONE_MINUS * RADIUS[1] * SIN_ANGLE[0], ONE_MINUS * YCEN + RADIUS[1] * COS_ANGLE[0], INSIDE },

        { XCEN + ONE_PLUS * RADIUS[0] * COS_ANGLE[0], ONE_PLUS * YCEN + RADIUS[0] * SIN_ANGLE[0], OUTSIDE },
        { XCEN - ONE_PLUS * RADIUS[1] * SIN_ANGLE[0], ONE_PLUS * YCEN + RADIUS[1] * COS_ANGLE[0], OUTSIDE }

        );

    
    double  xbnd_2 = sqrt( ( RADIUS[0] * COS_ANGLE[0] ) * ( RADIUS[0] * COS_ANGLE[0] )
                           +
                           ( RADIUS[1] * SIN_ANGLE[0] ) * ( RADIUS[1] * SIN_ANGLE[0] ) );

    double  ybnd_2 = sqrt( ( RADIUS[0] * SIN_ANGLE[0] ) * ( RADIUS[0] * SIN_ANGLE[0] )
                           +
                           ( RADIUS[1] * COS_ANGLE[0] ) * ( RADIUS[1] * COS_ANGLE[0] ) );

    XBND_( XCEN - xbnd_2, XCEN - xbnd_2 );
    YBND_( YCEN - ybnd_2, YCEN - ybnd_2 );

    DSL_( "Ellipse(%g,%g,%g,%g,%g)", XCEN,YCEN,RADIUS[0],RADIUS[1], ANGLE[0] );

#include "ellipse.h"

    FINALIZE_SETUP;
}

void test_factory( ) {
    common_test_factory( setup() );
}

void test_dsl() {
    common_test_dsl( setup() );
}


TEST_LIST = {

    { "factory", test_factory },
    { "dsl", test_dsl },
    { NULL, NULL }
};
