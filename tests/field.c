#include <math.h>
#include <float.h>
#include <stddef.h>

#include "acutest.h"
#include "cxcregion.h"
#include "region_priv.h"

#include "common.h"

Setup
setup () {

    INIT_SETUP( "Field" );

    SHAPE_AREA_( 0 );
    REGION_AREA_( DBL_MAX );
    DSL_( "Field()" );

    MASK_();
    FINALIZE_SETUP;
}
void test_factory( ) {
    common_test_factory( setup() );
}

void test_dsl() {
    common_test_dsl( setup() );
}


TEST_LIST = {

    { "factory", test_factory },
    { "dsl", test_dsl },
    { NULL, NULL }
};
