#include <float.h>
#include <math.h>

#include "acutest.h"

#include "cxcregion.h"
#include "region_priv.h"

#include "common.h"

Setup
setup () {

    INIT_SETUP( "Pie" );

    XCEN_( 10 );
    YCEN_( 8 );
    XPOS_( XCEN );
    YPOS_( YCEN );
    RADIUS_( 11, 33 );
    ANGLE_(  44, 55 );

    const double COS_ANGLE[]       = { COS(ANGLE[0])        , COS(ANGLE[1])        };
    const double SIN_ANGLE[]       = { SIN(ANGLE[0])        , SIN(ANGLE[1])        };

    const double ANGLE_MINUS[]     = { ONE_MINUS * ANGLE[0] , ONE_MINUS * ANGLE[1] };
    const double COS_ANGLE_MINUS[] = { COS(ANGLE_MINUS[0])  , COS(ANGLE_MINUS[1])  };
    const double SIN_ANGLE_MINUS[] = { SIN(ANGLE_MINUS[0])  , SIN(ANGLE_MINUS[1])  };

    const double ANGLE_PLUS[]      = { ONE_PLUS  * ANGLE[0] , ONE_PLUS * ANGLE[1]  };
    const double COS_ANGLE_PLUS[]  = { COS(ANGLE_PLUS[0])   , COS(ANGLE_PLUS[1])   };
    const double SIN_ANGLE_PLUS[]  = { SIN(ANGLE_PLUS[0])   , SIN(ANGLE_PLUS[1])   };

    const double RADIUS_MINUS[]     = { ONE_MINUS * RADIUS[0] , ONE_MINUS * RADIUS[1] };
    const double RADIUS_PLUS[]      = { ONE_PLUS  * RADIUS[0] , ONE_PLUS * RADIUS[1]  };


    AREA_(  (ANGLE[1] - ANGLE[0] ) / 360 * M_PI * ( RADIUS[1]*RADIUS[1] - RADIUS[0]*RADIUS[0] ) );

    XBND_( XCEN + RADIUS[0] * COS_ANGLE[1], XCEN + RADIUS[1] * COS_ANGLE[0] );
    YBND_( YCEN + RADIUS[0] * SIN_ANGLE[0], YCEN + RADIUS[1] * SIN_ANGLE[1] );

    PTS_(
        { XCEN, YCEN, OUTSIDE },
        { XCEN + RADIUS[0] / 2, YCEN + RADIUS[0] / 2, OUTSIDE },  /* 45 degree angle */
        { XCEN + RADIUS[0], YCEN + RADIUS[0], INSIDE },  /* 45 degree angle */
        { XCEN + RADIUS[1], YCEN + RADIUS[1], OUTSIDE }, /* 45 degree angle */
        { XCEN + RADIUS[1], YCEN,  OUTSIDE }, /* 0 degree angle */
        { XCEN, YCEN + RADIUS[1],  OUTSIDE }, /* 90 degree angle */

        /* just inside the inner arc */
        { XCEN + RADIUS_PLUS[0] * COS_ANGLE_PLUS[0] , YCEN + RADIUS_PLUS[0] * SIN_ANGLE_PLUS[0],  INSIDE },
        { XCEN + RADIUS_PLUS[0] * COS_ANGLE_MINUS[1], YCEN + RADIUS_PLUS[0] * SIN_ANGLE_MINUS[1],  INSIDE },

        /* just inside the outer arc */
        { XCEN + RADIUS_MINUS[1] * COS_ANGLE_PLUS[0] , YCEN + RADIUS_MINUS[1] * SIN_ANGLE_PLUS[0],  INSIDE },
        { XCEN + RADIUS_MINUS[1] * COS_ANGLE_MINUS[1], YCEN + RADIUS_MINUS[1] * SIN_ANGLE_MINUS[1],  INSIDE },

        /* just outside the inner arc */
        { XCEN + RADIUS_MINUS[0] * COS_ANGLE_MINUS[0], YCEN + RADIUS_MINUS[0] * SIN_ANGLE_MINUS[0],  OUTSIDE },
        { XCEN + RADIUS_MINUS[0] * COS_ANGLE_MINUS[1], YCEN + RADIUS_MINUS[0] * SIN_ANGLE_MINUS[1],  OUTSIDE },

        /* just outside the outer arc */
        { XCEN + RADIUS_PLUS[1] * COS_ANGLE_MINUS[0], YCEN + RADIUS_PLUS[1] * SIN_ANGLE_MINUS[0],  OUTSIDE },
        { XCEN + RADIUS_PLUS[1] * COS_ANGLE_PLUS[1], YCEN + RADIUS_PLUS[1] * SIN_ANGLE_PLUS[1],  OUTSIDE },

        );


    DSL_( "Pie(%g,%g,%g,%g,%g,%g)", XCEN,YCEN,RADIUS[0],RADIUS[1],ANGLE[0],ANGLE[1] );


#include "pie.h"

    FINALIZE_SETUP;
}

void test_factory( ) {
    common_test_factory( setup() );
}

void test_dsl() {
    common_test_dsl( setup() );
}


TEST_LIST = {

    { "factory", test_factory },
    { "dsl", test_dsl },
    { NULL, NULL }
};
