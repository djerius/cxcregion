#include <stddef.h>

#include "acutest.h"
#include "cxcregion.h"
#include "region_priv.h"

#include "common.h"

Setup
setup () {

    INIT_SETUP( "Point" );

    XCEN_(8);
    YCEN_(10);

    AREA_( 0 );
    XPOS_( XCEN );
    YPOS_( YCEN );
    XBND_( XCEN, XCEN );
    YBND_( YCEN, YCEN );
    PTS_( {XCEN, YCEN, INSIDE},
          {XCEN-1, YCEN+1, OUTSIDE}
        );

#include "point.h"
    DSL_( "Point(%g,%g)", XCEN, YCEN);

    FINALIZE_SETUP;
}

void test_factory( ) {
    common_test_factory( setup() );
}

void test_dsl() {
    common_test_dsl( setup() );
}


TEST_LIST = {

    { "factory", test_factory },
    { "dsl", test_dsl },
    { NULL, NULL }
};
