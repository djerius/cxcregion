#ifndef POINT_H
#define POINT_H
#include "common.h"

static const short mask_val[] = {
0, 0, 0,
0, 1, 0,
0, 0, 0,
};
static const Mask MASK = { mask_val, 3, 3};

#endif /* ! POINT_H */
