#include <float.h>
#include <math.h>

#include "acutest.h"
#include "cxcregion.h"
#include "region_priv.h"

#include "common.h"


Setup
setup () {

    INIT_SETUP( "Rectangle" );

    XMIN_( -11 );
    XMAX_( 14 );
    YMIN_( -22 );
    YMAX_( 99 );

    XLEN_( XMAX - XMIN );
    YLEN_( YMAX - YMIN );

    XPOS_( XMIN, XMAX );
    YPOS_( YMIN, YMAX );

    XCEN_( XMIN + XLEN / 2 );
    YCEN_( YMIN + YLEN / 2 );

    RADIUS_();
    ANGLE_();

    AREA_( XLEN * YLEN );
    XBND_( XMIN, XMAX );
    YBND_( YMIN, YMAX );
    PTS_(

        { XCEN, YCEN, INSIDE },
        { XMIN, YMIN, INSIDE },
        { XMIN, YMAX, INSIDE },
        { XMAX, YMIN, INSIDE },
        { XMAX, YMAX, INSIDE },

        { XMIN-1, YMIN-1, OUTSIDE },
        { XMIN-1, YMAX+1, OUTSIDE },
        { XMAX+1, YMIN-1, OUTSIDE },
        { XMAX+1, YMAX+1, OUTSIDE },

        );
#include "rectangle.h"

    DSL_( "Rectangle(%g,%g,%g,%g)", XPOS[0],YPOS[0],XPOS[1],YPOS[1] );

    FINALIZE_SETUP;
}

void test_factory( ) {
    common_test_factory( setup() );
}

void test_dsl() {
    common_test_dsl( setup() );
}

TEST_LIST = {
    { "factory", test_factory },
    { "dsl", test_dsl },
    { NULL, NULL }
};
