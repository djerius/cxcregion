#include <float.h>
#include <math.h>

#include "acutest.h"

#include "cxcregion.h"
#include "region_priv.h"

#include "common.h"

Setup
setup () {

    INIT_SETUP( "Sector" );

    XCEN_( 10 );
    YCEN_( 8 );

    XPOS_( XCEN );
    YPOS_( YCEN );
    ANGLE_( 44, 55 );

    const double COS_ANGLE[]       = { COS(ANGLE[0])        , COS(ANGLE[1])        };
    const double SIN_ANGLE[]       = { SIN(ANGLE[0])        , SIN(ANGLE[1])        };

    const double ANGLE_MINUS[]     = { ONE_MINUS * ANGLE[0] , ONE_MINUS * ANGLE[1] };
    const double COS_ANGLE_MINUS[] = { COS(ANGLE_MINUS[0])  , COS(ANGLE_MINUS[1])  };
    const double SIN_ANGLE_MINUS[] = { SIN(ANGLE_MINUS[0])  , SIN(ANGLE_MINUS[1])  };

    const double ANGLE_PLUS[]      = { ONE_PLUS  * ANGLE[0] , ONE_PLUS * ANGLE[1]  };
    const double COS_ANGLE_PLUS[]  = { COS(ANGLE_PLUS[0])   , COS(ANGLE_PLUS[1])   };
    const double SIN_ANGLE_PLUS[]  = { SIN(ANGLE_PLUS[0])   , SIN(ANGLE_PLUS[1])   };

    AREA_( 1 );

    const double TRADIUS = 10;

    PTS_( { XCEN, YCEN, INSIDE },
          { XCEN + 10, YCEN+ 10, INSIDE },  /* 45 degree angle */

          /* fudge for floating point hilarity */

          /* just inside the boundary */
          { XCEN + TRADIUS * COS_ANGLE_PLUS[0] , YCEN + TRADIUS * SIN_ANGLE_PLUS[0],  INSIDE },
          { XCEN + TRADIUS * COS_ANGLE_MINUS[1], YCEN + TRADIUS * SIN_ANGLE_MINUS[1], INSIDE },

          /* just outside the boundary */
          { XCEN + TRADIUS * COS_ANGLE_MINUS[0], YCEN + TRADIUS * SIN_ANGLE_MINUS[0], OUTSIDE },
          { XCEN + TRADIUS * COS_ANGLE_PLUS[1],  YCEN + TRADIUS * SIN_ANGLE_PLUS[1],  OUTSIDE },
        );

    MASK_();

    DSL_( "Sector(%g,%g,%g,%g)", XCEN,YCEN,ANGLE[0],ANGLE[1] );

    FINALIZE_SETUP;
}

void test_factory( ) {
    common_test_factory( setup() );
}

void test_dsl() {
    common_test_dsl( setup() );
}


TEST_LIST = {

    { "factory", test_factory },
    { "dsl", test_dsl },
    { NULL, NULL }
};
